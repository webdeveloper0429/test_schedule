'use strict';

var counter = 0,
    schedule = require('node-schedule'),
    taskSchedule = new schedule.RecurrenceRule();

//schedule the task to run the first, second, third, fourth and fifth minute after, every hour.
taskSchedule.minute = [0,30];

//this function is executed on every iteration of the schedule
function reportOnSchdeule () {
	//increment the counter
	var dateForLog = new Date();

	//report that the scheduled task ran
    console.log(dateForLog);
}

//Initialze the schedule
schedule.scheduleJob(taskSchedule, reportOnSchdeule);

//just so we know the code is running, while we wait for the first iteration to run
console.log('every 30 min console');